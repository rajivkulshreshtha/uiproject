package com.example.rajiv.uiproject.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rajiv.uiproject.R;

import java.util.Arrays;
import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> {

    private Context mContext;
    private View view;
    private List<String> songList;
    private List<String> artistList;


    public SongAdapter(Context mContext) {
        this.mContext = mContext;

        songList = Arrays.asList(mContext.getResources().getStringArray(R.array.query_suggestions));
        artistList = Arrays.asList(mContext.getResources().getStringArray(R.array.artist_array));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.adapter_song, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.songName.setText(songList.get(position));
        holder.artistName.setText(artistList.get(position));

    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView songName, artistName;

        public ViewHolder(View itemView) {
            super(itemView);

            songName = itemView.findViewById(R.id.song_name_textview);
            artistName = itemView.findViewById(R.id.artist_textview);

        }
    }
}
