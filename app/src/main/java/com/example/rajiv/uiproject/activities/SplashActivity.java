package com.example.rajiv.uiproject.activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.VideoView;

import com.example.rajiv.uiproject.R;
import com.example.rajiv.uiproject.utils.Typewriter;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class SplashActivity extends AppCompatActivity {

    private Typewriter typewriter;
    private VideoView myVideoView;
    private String str = "Welcome to\nHRC Karaoke Nights\nWith Artistivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        myVideoView = (VideoView) findViewById(R.id.video_view);
        typewriter = findViewById(R.id.type_textview);

    }

    private void initAnim() {

        if (typewriter != null && myVideoView != null) {
            myVideoView.start();
            try {
                myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash));
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            myVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.seekTo(12000);
                    mp.start();
                }
            });

            typewriter.setCharacterDelay(110);
            typewriter.animateText(changeColorAndBold(str, 0, 10, 10, 29));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initAnim();
    }

    @Override
    protected void onPause() {
        super.onPause();
        myVideoView.pause();

    }

    public Spannable changeColorAndBold(String str, int boldStartIndex, int boldStopIndex, int colorStartIndex, int colorStopIndex) {
        SpannableStringBuilder spannable = new SpannableStringBuilder(str);
        spannable.setSpan(new RelativeSizeSpan(1.8f), boldStartIndex, boldStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), boldStartIndex, boldStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimary)), colorStartIndex, colorStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new RelativeSizeSpan(1.8f), colorStartIndex, colorStopIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)), colorStopIndex + 1, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void register(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void login(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
