package com.example.rajiv.uiproject.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.example.rajiv.uiproject.R;
import com.example.rajiv.uiproject.activities.MainActivity;
import com.example.rajiv.uiproject.activities.RegisterActivity;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    public void performLogin(View view) {
        startActivity(new Intent(this, MainActivity.class));

    }

    public void register(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

}
